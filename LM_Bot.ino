#include "Lidar.h"
#include "Sonar.h"
#include "motor.h"

#include <Servo.h>
#include "SharpIR.h"
#include <Ultrasonic.h>


#define LIDAR_SERVO_PIN    (11)
#define SONAR_TRIGGER_PIN  (12)
#define SONAR_ECHO_PIN     (13)

// ************* Globals ***********************
int leftWheelEncoder;
int rightWheelEncoder;

Lidar lidar;

void setup()
{
	Serial.begin(9600);
	Serial1.begin(38400); // Robo claw
	Serial3.begin(48000); // Voice

	lidar.init(A0, 10, LIDAR_SERVO_PIN);
	RoboClaw.init(50, 51, 52, 53, &Serial1);

	Serial.println("Init complete");

}

void loop()
{
// 	RoboClaw.stop(BOTH_MOTORS);
// 	Serial.println("Gonna move inches!");
// 	RoboClaw.moveInches(-3, RIGHT_MOTOR);
// 	
// 	//RoboClaw.moveInches(3, RIGHT_MOTOR);
// 	
// 	//RoboClaw.moveInches(-10, LEFT_MOTOR);
// 	//RoboClaw.moveInches(10, LEFT_MOTOR);
// 	
// 	while(1){}
// 	
// 	while(Serial3.available()){
// 		Serial.println("Voice!");
// 		char c;
// 		c = Serial3.read();
// 		Serial.println(c);
// 	}
// 		
	
	float distInches;

	// This moves each sonar to it's next location
	// and takes a measurement
	lidar.sweep();
	
	// Read the distance straight ahead, left side
//	distInches = sonar.rangeInches(90);
//  	Serial.print(", IN: ");
//  	Serial.println(distInches);
//	// Close object, evasive action
	if(distInches < 18){
		Serial.println("Reverse Right!!");
		Serial1.write(255);
// 		RoboClaw.moveInches(-1, LEFT_MOTOR);
// 		RoboClaw.moveInches(-3, RIGHT_MOTOR);
	}
	else{
		Serial.println("AHEAD FULL!!");
		Serial1.write(127);
		Serial1.write(1);

		//RoboClaw.move(FORWARD);
	}
}
