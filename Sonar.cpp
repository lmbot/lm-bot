// 
// 
// 

#include "Sonar.h"
#include <Servo.h>
#include <Ultrasonic.h>
#include "SharpIR.h"

Sonar::Sonar()
{
	bzero((void*)rangeInchesAtDeg, sizeof(rangeInchesAtDeg));
}

void Sonar::init(int servoPin, int echoPin, int trigPin)
{
	_curAngle = 0;
	// Create the servo and attach it to it's pin
	servoPtr = new Servo;
	servoPtr->attach(servoPin);
	// Create the sensor and attach it to it's pin
	sensorPtr = new Ultrasonic(trigPin,echoPin);
	// Set default sweep limits
	minSweepDeg = 40;
	maxSweepDeg = 160;
	sweepInc = 30;
	numSweepDeg =  ((maxSweepDeg - minSweepDeg) / sweepInc) + 1;	
	_curAngle = minSweepDeg;
}

void Sonar::sweep()
{
	long microsec = 0.0;
	static int increment = sweepInc;
	
	// Move to current Angle
	//servoPtr->write(_curAngle);

	delay(100);
	
	if(SONAR_SIMULATION_ON)
	{
		rangeInchesAtDeg[_curAngle] = random(12,60);
	}
	else
	{
		// Record range at current deg
		microsec += sensorPtr->timing();
		rangeInchesAtDeg[_curAngle] = sensorPtr->convert(microsec, Ultrasonic::IN);
		Serial.print(rangeInchesAtDeg[_curAngle]);
		Serial.print(" ");
	}

	if(SONAR_DEBUG_ON)
	{
// 		Serial.print("Range: ");
// 		Serial.print(rangeInchesAtDeg[_curAngle]);
// 		Serial.print(" @ Deg:");
// 		Serial.println(_curAngle);
	}
	_curAngle += increment;
	
	if(_curAngle >= maxSweepDeg)
	{
		increment = -sweepInc;
// 		for(int i=minSweepDeg; i<=maxSweepDeg; i+=sweepInc)
// 		{
// 			Serial.print(" ");
// 			Serial.print(rangeInches(i));
// 			Serial.print(" ");
// 		}
// 		Serial.println(" ");
// 		Serial.println("***************************************************");
// 		Serial.println(" ");
	}	
	else if(_curAngle < minSweepDeg)
	{
		increment = sweepInc;
	}
}

void Sonar::setSweepLimits(int minDeg, int maxDeg)
{
	maxSweepDeg = maxDeg;
	minSweepDeg = minDeg;
	numSweepDeg = maxDeg - minDeg / sweepInc;
}

int Sonar::rangeInches(int angle)
{
	if(angle < minSweepDeg || angle > maxSweepDeg){
		return -1;
	}
	return rangeInchesAtDeg[angle];
}


