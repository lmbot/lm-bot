// Sonar.h

#ifndef _SONAR_h
#define _SONAR_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include "Servo.h"
#include "SharpIR.h"

#define SONAR_DEBUG_ON (true)
#define LIDAR_SIMULATION_ON (false)

#define MAX_DEGREES (360)

class Lidar
{
	public:
	Lidar(); // Constructor
	
	protected:
	int rangeInchesAtDeg[MAX_DEGREES];
	int _curAngle;
	Servo* servoPtr;
	SharpIR* sensorPtr;
	int maxSweepDeg;
	int minSweepDeg;
	int numSweepDeg;
	int sweepInc;
	int sensorPin;
	int aveNum;
	
	public:
	void init(int irPin, int avg, int servoPin);
	
	// Set the limits to sweep through
	void setSweepLimits(int minDeg, int maxDeg);
	
	// Move to next location and record distance
	void sweep();
	
	// Return the last recorded distance at a given angle
	int rangeInches(int angle);

};

#endif

