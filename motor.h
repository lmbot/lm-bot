/* 
* motor.h
*
* Created: 5/5/2015 9:46:39 PM
* Author: BrianBailey
*/


#ifndef __MOTOR_H__
#define __MOTOR_H__


#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#define LEFT_FULL_FORWARD  128
#define RIGHT_FULL_FORWARD 127

#define LEFT_FULL_REVERSE  255
#define RIGHT_FULL_REVERSE 1

#define LEFT_STOP  192
#define RIGHT_STOP 64

#define FULL_ROTATION_TICS (63600)
#define INCHES_PER_REVOLUTION (9.33)
#define TICS_PER_INCH (6817)

#define LEFT_MOTOR 0
#define RIGHT_MOTOR 1
#define BOTH_MOTORS 2

#define FORWARD 0
#define REVERSE 1

class RoboClawClass
{
//variables
public:
protected:
private:
	int lPinA;
	int lPinB;
	int rPinA;
	int rPinB;
	HardwareSerial* cmdPort;
	bool initialized;
	int leftEncoderCounter;
	int rightEncoderCounter;
	bool leftMoving;
	bool rightMoving;
	

//functions
public:
	RoboClawClass();
	~RoboClawClass();
	void init(int leftEncPinA, int leftEncPinB, int rightEncPinA, int rightEncPinB, HardwareSerial* cmdPort);
	void leftIsr();
	void rightIsr();
	void moveInches(int inches, int motor);
	void move(int direction);
	void stop(int motor);
	
protected:
private:
	RoboClawClass( const RoboClawClass &c );
	RoboClawClass& operator=( const RoboClawClass &c );

}; //motor

extern RoboClawClass RoboClaw;

#endif //__MOTOR_H__

