/* 
* motor.cpp
*
* Created: 5/5/2015 9:46:39 PM
* Author: BrianBailey
*/


#include "motor.h"

RoboClawClass RoboClaw;

//**************  ISR Routines  *****************

void leftWheelIsr()
{
	RoboClaw.leftIsr();
}

void rightWheelIsr()
{
	RoboClaw.rightIsr();
}


// default constructor
RoboClawClass::RoboClawClass()
{
	
}
	
void RoboClawClass::init(int leftEncPinA, int leftEncPinB, int rightEncPinA, int rightEncPinB, HardwareSerial* serialPtr)
{
	lPinA = leftEncPinA;
	lPinB = leftEncPinB;
	rPinA = rightEncPinA;
	rPinB = rightEncPinB;
	
	pinMode(lPinA, INPUT_PULLUP);
	pinMode(lPinB, INPUT_PULLUP);
	pinMode(rPinA, INPUT_PULLUP);
	pinMode(rPinB, INPUT_PULLUP);

	leftMoving = false;
	rightMoving = false;
		
	rightEncoderCounter = 0;
	leftEncoderCounter = 0;

// 	attachInterrupt(rPinA, rightWheelIsr, CHANGE);
//	attachInterrupt(lPinA, leftWheelIsr, CHANGE);
	cmdPort = serialPtr;

	initialized = true;	
} 

// default destructor
RoboClawClass::~RoboClawClass()
{
} //~motor

// Moves the motors a calculated number of turns to traverse given distance 
void RoboClawClass::moveInches(int inches, int motor)
{
	if(inches < 0)
	{
		if(motor == LEFT_MOTOR)
		{
			leftMoving = true;
			cmdPort->write(LEFT_FULL_REVERSE);
			leftEncoderCounter = inches * TICS_PER_INCH;
		}
		else if(motor == RIGHT_MOTOR)
		{
			rightMoving = true;			
			cmdPort->write(RIGHT_FULL_REVERSE);
			rightEncoderCounter = inches * TICS_PER_INCH;
		}
		else
		{
			leftMoving = rightMoving = true;			
			cmdPort->write(RIGHT_FULL_REVERSE);
			cmdPort->write(LEFT_FULL_REVERSE);
			rightEncoderCounter = leftEncoderCounter = inches * TICS_PER_INCH;
		}
	}
	else
	{
		if(motor == LEFT_MOTOR)
		{
			leftMoving = true;			
			cmdPort->write(LEFT_FULL_FORWARD);
			leftEncoderCounter = inches * TICS_PER_INCH;
		}
		else if(motor == RIGHT_MOTOR)
		{
			rightMoving = true;			
			cmdPort->write(RIGHT_FULL_FORWARD);
			rightEncoderCounter = inches * TICS_PER_INCH;
		}
		else
		{
			leftMoving = rightMoving = true;			
			cmdPort->write(RIGHT_FULL_FORWARD);
			cmdPort->write(LEFT_FULL_FORWARD);
			rightEncoderCounter = leftEncoderCounter = inches * TICS_PER_INCH;
		}
	}
	Serial.print("Left Encoder Count: ");
	Serial.println(leftEncoderCounter);
	Serial.print("Right Encoder Count: ");
	Serial.println(rightEncoderCounter);
}

void RoboClawClass::stop(int motor)
{
	if(motor == RIGHT_MOTOR || motor == BOTH_MOTORS)
	{
		cmdPort->write(RIGHT_STOP);
		rightMoving = false;
	}
	else if (motor == LEFT_MOTOR)
	{
		cmdPort->write(LEFT_STOP);
		leftMoving = false;
	}
	else{
		cmdPort->write(RIGHT_STOP);
		cmdPort->write(LEFT_STOP);		
		rightMoving = false;
		leftMoving = false;
	}
//	Serial.println("Stopping motor!");
}

void RoboClawClass::leftIsr()
{
	if(leftEncoderCounter == 0 && !leftMoving){
		stop(LEFT_MOTOR);
		return;
	}
	if(digitalRead(lPinA) == digitalRead(lPinB))
	{
		leftEncoderCounter++;
	}
	else
	{
		leftEncoderCounter--;
	}
}

void RoboClawClass::rightIsr()
{
	if(rightEncoderCounter == 0 && !rightMoving){
		stop(RIGHT_STOP);
		return;
	}
	if(digitalRead(rPinA) == digitalRead(rPinB))
	{
		rightEncoderCounter++;
	}
	else
	{
		rightEncoderCounter--;
	}
}

void RoboClawClass::move(int direction)
{
	leftMoving = rightMoving = true;	
	if(direction == FORWARD)
	{
		cmdPort->write(RIGHT_FULL_FORWARD);
		cmdPort->write(LEFT_FULL_FORWARD);
	}
	if(direction == REVERSE)
	{
		cmdPort->write(RIGHT_FULL_REVERSE);
		cmdPort->write(LEFT_FULL_REVERSE);
	}
}

