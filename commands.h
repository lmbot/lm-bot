#ifndef COMMAND_H_
#define COMMAND_H_

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include "CSI.h"

#define SHOW_HELP_MENU true

/***********************************************************
* Command characters
* These are implementation dependent
***********************************************************/
// Command       Char     Description                       Examples
//--------------------------------------------------------------------
#define VERSION		 'V'  // Get version                    	@GV#
#define STOP		 'S'  // Stop                       		@SS#
#define FORWARD		 'F'  // Forward	                    	@SF#
#define REVERSE		 'B'  // Reverse	                   		@SB#
#define LEFT		 'L'  // LEFT                               @SL#
#define RIGHT		 'R'  // RIGHT								@SR#

char* unsupportedCmd(void* vui, char* cmd);
#define NOOP unsupportedCmd

void showCmdList(HardwareSerial* cmdPort);

char* setLeft(void* vui, char* cmd);

char* setRight(void* vui, char* cmd);

char* setStop(void* vui, char* cmd);

char* setForward(void* vui, char* cmd);

char* setReverse(void* vui, char* cmd);

char* getVersion(void* vui, char* cmd);

Command* getCmdList();
int getCmdListLen();

#endif
