//
//
//

#include "Lidar.h"
#include <Servo.h>
#include "SharpIR.h"

Lidar::Lidar()
{
	bzero((void*)rangeInchesAtDeg, sizeof(rangeInchesAtDeg));
}

void Lidar::init(int irPin, int ave, int servoPin)
{
	_curAngle = 0;
	// Create the servo and attach it to it's pin
	// Create the servo and attach it to it's pin
	servoPtr = new Servo;
	servoPtr->attach(servoPin);
	
	// Create the sensor and attach it to it's pin
	sensorPtr = new SharpIR(irPin,ave,93,20150);
	
	// Set default sweep limits
	minSweepDeg = 60;
	maxSweepDeg = 180;
	sweepInc = 2;
	numSweepDeg =  ((maxSweepDeg - minSweepDeg) / sweepInc) + 1;
	_curAngle = minSweepDeg;
}

void Lidar::sweep()
{
	long microsec = 0.0;
	static int increment = sweepInc;
	
	// Move to current Angle
	servoPtr->write(_curAngle);

	delay(10);
	
	if(LIDAR_SIMULATION_ON)
	{
		rangeInchesAtDeg[_curAngle] = random(12,60);
	}
	else
	{
		// Record range at current deg
		rangeInchesAtDeg[_curAngle] = sensorPtr->distance();
		Serial.print(rangeInchesAtDeg[_curAngle]);
		Serial.print(" ");
	}

	if(SONAR_DEBUG_ON)
	{
		Serial.print("Range: ");
		Serial.print(rangeInchesAtDeg[_curAngle]);
		Serial.print(" @ Deg:");
		Serial.println(_curAngle);
	}
	_curAngle += increment;
	
	if(_curAngle >= maxSweepDeg)
	{
		increment = -sweepInc;
		// 		for(int i=minSweepDeg; i<=maxSweepDeg; i+=sweepInc)
		// 		{
		// 			Serial.print(" ");
		// 			Serial.print(rangeInches(i));
		// 			Serial.print(" ");
		// 		}
		// 		Serial.println(" ");
		// 		Serial.println("***************************************************");
		// 		Serial.println(" ");
	}
	else if(_curAngle < minSweepDeg)
	{
		increment = sweepInc;
	}
}

void Lidar::setSweepLimits(int minDeg, int maxDeg)
{
	maxSweepDeg = maxDeg;
	minSweepDeg = minDeg;
	numSweepDeg = maxDeg - minDeg / sweepInc;
}

int Lidar::rangeInches(int angle)
{
	if(angle < minSweepDeg || angle > maxSweepDeg){
		return -1;
	}
	return rangeInchesAtDeg[angle];
}


