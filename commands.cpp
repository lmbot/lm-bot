
/******************************************************
*
* commands.cpp
*
* Design Mentor Copyright 2015
*
* Brian Bailey - 1/20/2015
*
*
********************************************************/

#include "globals.h"
#include "commands.h"
#include "CSI.h"

Command cmdList[] = {
	/* Device ,  Getter Func,   Setter Func */
	/***************************************/
	{VERSION,       getVersion,     NOOP,			"Print current firmware version. Args: None"},
	{STOP,			NOOP,			setStop,		"Stop"},
	{FORWARD,		NOOP,			setForward,		"Forward"},
	{REVERSE,		NOOP,			setReverse,		"Reverse"},
	{LEFT,			NOOP,			setLeft,		"Left"},
	{RIGHT,			NOOP,			setRight,		"Right"}
};



Command* getCmdList()
{
	return cmdList;
}

int getCmdListLen()
{
	return sizeof(cmdList)/sizeof(Command);
}

char* unsupportedCmd(void* vui, char* cmd)
{
	CSI* ui = (CSI*)vui;
	ui->cmdPort->print("Unsupported command for ");
	ui->cmdPort->print(char(cmd[1]));
	ui->cmdPort->print("?");
	showCmdList(ui->cmdPort);
	return cmd;
}

char* setStop(void* vui, char* cmd) {
	//TODO stop
}

char* setForward(void* vui, char* cmd) {
	CSI* ui = (CSI*)vui;
		int nArgs = 0;
	struct ARGS {
		int distance;
	} args;
	nArgs = ui->getParams(cmd, (int*)&args, sizeof(ARGS) / sizeof(int));
	if (nArgs >= 1 /*TODO set max travel distance*/) {
		//TODO move forward
	}
	else {
		ui->cmdPort->print("Numeric argument required");
		showCmdList(ui->cmdPort);
	}

}

char* setReverse(void* vui, char* cmd) {
	CSI* ui = (CSI*)vui;
		int nArgs = 0;
	struct ARGS {
		int distance;
	} args;
	nArgs = ui->getParams(cmd, (int*)&args, sizeof(ARGS) / sizeof(int));
	if (nArgs >= 1 /*TODO set max travel distance*/) {
		//TODO move backward
	}
	else {
		ui->cmdPort->print("Numeric argument required");
		showCmdList(ui->cmdPort);
	}

}

char* setLeft(void* vui, char* cmd) {
	CSI* ui = (CSI*)vui;
		int nArgs = 0;
	struct ARGS {
		int distance;
	} args;
	nArgs = ui->getParams(cmd, (int*)&args, sizeof(ARGS) / sizeof(int));
	if (nArgs >= 1 /*TODO set max travel distance*/) {
		//TODO move left (i.e. one motor on or both motors in opposite directions to pivot)
	}
	else {
		ui->cmdPort->print("Numeric argument required");
		showCmdList(ui->cmdPort);
	}

}

char* setRight(void* vui, char* cmd) {
	CSI* ui = (CSI*)vui;
		int nArgs = 0;
	struct ARGS {
		int distance;
	} args;
	nArgs = ui->getParams(cmd, (int*)&args, sizeof(ARGS) / sizeof(int));
	if (nArgs >= 1 /*TODO set max travel distance*/) {
		//TODO move right (i.e. one motor on or both motors in opposite directions to pivot)
	}
	else {
		ui->cmdPort->print("Numeric argument required");
		showCmdList(ui->cmdPort);
	}

}